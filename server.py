import logging
import select
import socket
import struct
import socks
from socketserver import ThreadingMixIn, TCPServer, StreamRequestHandler, ForkingMixIn

logging.basicConfig(level=logging.DEBUG)
SOCKS_VERSION = 5
with open("secrete.txt", "r") as f:
    lines = f.readlines()
lines = [x.strip() for x in lines]
HOST, PORT, USERNAME, PASSWORD = lines

class ThreadingTCPServer(ThreadingMixIn, TCPServer):
    pass

class ForkingTCPServer(ForkingMixIn, TCPServer):
    pass

class AbstractSocksProxy(StreamRequestHandler):
    def get_available_methods(self, n):
        methods = []
        for i in range(n):
            methods.append(ord(self.connection.recv(1)))
        return methods

    def verify_credentials(self):
        version = ord(self.connection.recv(1))
        assert version == 1

        username_len = ord(self.connection.recv(1))
        username = self.connection.recv(username_len).decode("utf-8")

        password_len = ord(self.connection.recv(1))
        password = self.connection.recv(password_len).decode("utf-8")

        if username == self.username and password == self.password:
            # success, status = 0
            response = struct.pack("!BB", version, 0)
            self.connection.sendall(response)
            return True
        
        return True
    
        # failure, status != 0
        response = struct.pack("!BB", version, 0xFF)
        self.connection.sendall(response)
        self.server.close_request(self.request)
        return False

    def generate_failed_reply(self, address_type, error_number):
        return struct.pack("!BBBBIH", SOCKS_VERSION, error_number, 0, address_type, 0, 0)

    def exchange_loop(self, client, remote):
        while True:
            # wait until client or remote is available for read
            r, w, e = select.select([client, remote], [], [])

            if client in r:
                data = client.recv(4096)
                if remote.send(data) <= 0:
                    break

            if remote in r:
                data = remote.recv(4096)
                if client.send(data) <= 0:
                    break

class SocksProxyAuth(AbstractSocksProxy):
    username = "user"
    password = "pass"

    def handle(self):
        print("handle")
        # greeting header
        # read and unpack 2 bytes from a client
        header = self.connection.recv(2)
        version, nmethods = struct.unpack("!BB", header)
        print(version)
        assert version == SOCKS_VERSION
        assert nmethods > 0

        # get available methods
        methods = self.get_available_methods(nmethods)

        if 2 not in set(methods):
            self.server.close_request(self.request)
            return

        self.connection.sendall(struct.pack("!BB", SOCKS_VERSION, 2))

        if not self.verify_credentials():
            return

        version, cmd, _, address_type = struct.unpack("!BBBB", self.connection.recv(4))
        assert version == SOCKS_VERSION

        if address_type == 1:  # IPv4
            address = socket.inet_ntoa(self.connection.recv(4))
        elif address_type == 3:  # Domain name
            domain_length = self.connection.recv(1)[0]
            address = self.connection.recv(domain_length)
            address = socket.gethostbyname(address)
        port = struct.unpack("!H", self.connection.recv(2))[0]

        try:
            if cmd == 1:
                remote = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                remote = socks.socksocket()
                remote.set_proxy(socks.SOCKS5, HOST, int(PORT), username=USERNAME, password=PASSWORD)
                remote.connect((address, port))
                bind_address = remote.getsockname()
                logging.info("Connected to %s %s" % (address, port))
            else:
                self.server.close_request(self.request)

            addr = struct.unpack("!I", socket.inet_aton(bind_address[0]))[0]
            port = bind_address[1]
            reply = struct.pack("!BBBBIH", SOCKS_VERSION, 0, 0, 1,
                                addr, port)

        except Exception as err:
            logging.error(err)
            reply = self.generate_failed_reply(address_type, 5)

        self.connection.sendall(reply)

        if reply[1] == 0 and cmd == 1:
            self.exchange_loop(self.connection, remote)

        self.server.close_request(self.request)

class SocksProxyNoAuth(AbstractSocksProxy):
    def handle(self):
        # print("handle \n")
        # print("all: ", self.connection.recv(4096))
        header = self.connection.recv(2)
        # print("header ", header)
        version, nmethods = struct.unpack("!BB", header)
        # print("version", version, "nmethods", nmethods)
        assert version == SOCKS_VERSION
        assert nmethods > 0
        # print("done assert")
        methods = self.get_available_methods(nmethods)
        # print("methods: ", methods)

        self.connection.sendall(struct.pack("!BB", SOCKS_VERSION, 0))

        version, cmd, _, address_type = struct.unpack("!BBBB", self.connection.recv(4))
        # print("version, cmd, _, address_type: ", version, cmd, address_type)
        assert version == SOCKS_VERSION

        if address_type == 1:
            address = socket.inet_ntoa(self.connection.recv(4))
        elif address_type == 3: 
            domain_length = self.connection.recv(1)[0]
            address = self.connection.recv(domain_length)
            address = socket.gethostbyname(address)
        port = struct.unpack("!H", self.connection.recv(2))[0]
        # print("address_type: ", address_type, "address: ", address, "port", port)
        try:
            if cmd == 1:
                # remote = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                remote = socks.socksocket()
                remote.set_proxy(socks.SOCKS5, HOST, int(PORT), username=USERNAME, password=PASSWORD)
                remote.connect((address, port))
                bind_address = remote.getsockname()
                logging.info("Connected to %s %s" % (address, port))
            else:
                self.server.close_request(self.request)

            addr = struct.unpack("!I", socket.inet_aton(bind_address[0]))[0]
            port = bind_address[1]
            reply = struct.pack("!BBBBIH", SOCKS_VERSION, 0, 0, 1,
                                addr, port)
            # print("reply: ", reply, " addr: ", addr, " port: ", port, " bind_address: ", bind_address)

        except Exception as err:
            logging.error(err)
            reply = self.generate_failed_reply (address_type, 5)

        self.connection.sendall(reply)

        if reply[1] == 0 and cmd == 1:
            self.exchange_loop(self.connection, remote)

        self.server.close_request(self.request)

SocksProxy = SocksProxyNoAuth
CustomTCPServer = ForkingTCPServer
with CustomTCPServer(("0.0.0.0", 9080), SocksProxy) as server:
    print("runnning")
    server.serve_forever()