# FROM tiangolo/uwsgi-nginx:python3.9
FROM ubuntu/nginx:1.18-20.04_beta
# FROM nginx

RUN apt-get update && \
    apt-get install --no-install-recommends -y python3.9 python3-pip python3-wheel python3.9-dev
#    python3.9-venv build-essential && \
#    apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

COPY ./app /app

EXPOSE 9090
EXPOSE 5040
EXPOSE 80

CMD ["python3", "main.py"]
# CMD ["shadowproxy", "-v", "$SPARG"]

